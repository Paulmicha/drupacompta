Drupacompta
===========

[WIP] Finance manager based on Drupal 8.
Built with [Composer template for Drupal projects](https://github.com/drupal-composer/drupal-project).


## Getting started

### Prerequisites

- [Docker](https://docs.docker.com/engine/installation)
- [docker compose](https://docs.docker.com/compose/install)

See my [local VM installation notes](https://github.com/Paulmicha/debian-quickup/blob/master/Local%20Dev/Debian8_Docker_Compose.notes.sh).

### Project setup

- Clone this repo and `cd` into project dir
- Set permissions (as root) `chmod u+x ./scripts/d4d/fixperms.sh && ./scripts/d4d/fixperms.sh`
- Launch services : `./scripts/d4d/start.sh`
- Load CLI aliases : `source ./scripts/d4d/aliases.sh`
- Install Drupal 8 dependencies : `composer install`
- Install Drupal site : `drush si --db-url=mysql://drupal:drupal@mariadb:3306/drupal --site-name=Drupacompta --site-mail="site.email@domain.com" --account-name=admin --account-pass=admin --account-mail="your.email@domain.com" -y`
- [todo] Enable project's main feature (automatically creates content types etc.): `drush en drupacompta_f_main -y`
- [todo] Optional : generate sample data for quick demo.


## Usage instructions

### Local DB operations

- Make local backup of DB : `source ./scripts/utils/create_dump.sh`
- Restore last local backup of DB : `source ./scripts/utils/restore_last_dump.sh`

### Contrib modules management

- Download new contrib module : `composer require drupal/adminimal_admin_toolbar`
- Enable contrib module : `drush en adminimal_admin_toolbar -y`


## Current status

I'm hoping to get a working prototype around april 2017.


## License

Source code is [GPLv2](LICENSE).
All content is [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/), unless otherwise stated.
