#!/bin/bash

##
# Local permissions for Docker4Drupal.
#
# Run as sudo or root.
#
# Usage from project root dir :
# $ source scripts/d4d/fixperms.sh
#

chown 82:82 . -R
chmod +x scripts -R
chmod -wx web/sites/default/settings.php
chmod +w web/sites/default/files -R
chmod 1771 private -R
