#!/bin/bash

##
# Stop Docker4Drupal containers.
#
# Run as sudo or root.
#
# Usage from project root dir :
# $ ./scripts/d4d/stop.sh
#

docker-compose stop
