#!/bin/bash

##
# Make common aliases.
#
# Run as sudo or root.
#
# Usage from project root dir :
# $ source ./scripts/d4d/aliases.sh
#

alias composer="docker-compose exec --user 82 php composer"
alias drush="docker-compose exec --user 82 php drush --root=/var/www/html/web"

# See https://github.com/hechoendrupal/drupal-console/issues/2515
# alias drupal="docker-compose exec --user 82 php drupal --root=/var/www/html/web"
alias drupal="docker-compose exec --user 82 php ./vendor/drupal/console/bin/drupal --root=/var/www/html/web"
