#!/bin/bash

##
# Start Docker4Drupal containers.
#
# Run as sudo or root.
#
# Usage from project root dir :
# $ ./scripts/d4d/start.sh
#

docker-compose up -d
