#!/bin/bash

##
# Restore last local DB dump.
#
# Requires scripts/d4d/aliases.sh (aliases must already have been sourced).
# @todo automatically get INSTANCE or pass as argument.
# @see create_dump.sh
#
# Usage from project root dir :
# $ source ./scripts/utils/restore_last_dump.sh
#

INSTANCE="drupacompta.local"
DUMP_FILE_LAST="$HOME/$INSTANCE/dumps/last.sql.gz"

# Note : drush is an alias with relative path in 'web', and it doesn't have
# access to the host filesystem (outside Docker volume).
# So we need a temporary copy of the dump to restore it with drush.
cp $DUMP_FILE_LAST web/last.sql.gz

drush sql-drop -y
drush sql-query --file="last.sql.gz"

# Remove temporary copy.
# Note that drush sql-query leaves an unzipped file behind.
rm "web/last.sql"
