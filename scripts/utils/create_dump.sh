#!/bin/bash

##
# Make DB dump.
#
# Requires scripts/d4d/aliases.sh (aliases must already have been sourced).
# @todo automatically get $INSTANCE or pass as argument.
#
# Usage from project root dir :
# $ source ./scripts/utils/create_dump.sh
#

INSTANCE="drupacompta.local"
newrev=$(git log --pretty=format:'%H' -n 1)

DUMP_DIR="$HOME/$INSTANCE/dumps/$(date +%Y)/$(date +%m)/$(date +%d)"
DUMP_FILE_NAME="$(date +%H)-$(date +%M)-$(date +%S)-${newrev:0:8}.sql"
DUMP_FILE="${DUMP_DIR}/${DUMP_FILE_NAME}"
DUMP_FILE_LAST="$HOME/$INSTANCE/dumps/last.sql.gz"

if [ ! -d $DUMP_DIR ]; then
  mkdir -p $DUMP_DIR
fi

# Note : drush is an alias with relative path in 'web', and it doesn't have
# access to the host filesystem (outside Docker volume).
drush sql-dump --gzip --result-file=$DUMP_FILE_NAME --structure-tables-list="cache,cache_*,history,search_*,sessions,watchdog"

# Move the file outside the Docker volume.
mv "web/$DUMP_FILE_NAME.gz" "$DUMP_FILE.gz"

# Copy over as last dump for quicker restores.
# @see scripts/utils/restore_last_dump.sh
cp -f "$DUMP_FILE.gz" $DUMP_FILE_LAST
