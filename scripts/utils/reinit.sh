#!/bin/bash

##
# Reinit entirely local dev instance (restores to fresh install).
#
# Requires scripts/d4d/aliases.sh (aliases must already have been sourced).
#
# Update 2017-02-16 : Back to features-based rebuild attempts.
#
# Note 2017-02-14 :
# Previous attempt using the `drush si --config-dir=path/to/config/dir` option
# have failed. See https://github.com/drush-ops/drush/pull/1875
# Errors :
#   Unable to install the standard module since it does not exist.
#   Unable to uninstall the Minimal profile since it is the install profile.
# workaround : manually delete the line added in settings.php file during exec,
# but then got new Error: Call to a member function getCacheTags() on null in
# web/core/modules/shortcut/src/Entity/Shortcut.php on line 159
# → gave up, and used this approach instead :
# https://github.com/drush-ops/drush/pull/1635#issuecomment-150661519
#
# @param configuration key.
#
# Usage from project root dir :
# $ . scripts/utils/reinit.sh dev
# $ . scripts/utils/reinit.sh stage
# $ . scripts/utils/reinit.sh prod
#

# Param 1 is required.
# CONFIG_KEY=${1}
# if [ -z "${1}" ]; then
#   echo "Missing param 1 (config key). Usage ex: reinit.sh dev"
#   exit 1
# fi
#
# CONFIG_PATH="./config/$CONFIG_KEY"

drush sql-drop -y

drush si minimal --db-url=mysql://drupal:drupal@mariadb:3306/drupal --site-name="Drupacompta" --site-mail="site@email.com" --account-name=admin --account-pass=admin --account-mail="admin@email.com" -y

# To import configuration into fresh install, we need to reset UUID manually.
# UUID_FROM_CONFIG=$(cat $CONFIG_PATH/system.site.yml | grep uuid | tail -c +7 | head -c 36)
# drush config-set system.site uuid $UUID_FROM_CONFIG -y
#
# drush config-import $CONFIG_KEY -y

# ... still getting error :
# Drupal\Core\Config\ConfigImporterException: There were errors validating the config        [error]
# synchronization. in Drupal\Core\Config\ConfigImporter->validate() (line 728 of
# /var/www/html/web/core/lib/Drupal/Core/Config/ConfigImporter.php).
# The import failed due for the following reasons:                                           [error]
# Entities exist of type <em class="placeholder">Shortcut link</em> and <em
# class="placeholder"></em> <em class="placeholder">Default</em>. These entities need to
# be deleted before importing.
# → What now ?


# @evol see if features can be used to import "standalone" aspects of this
# project in other existing projects.
# Currently, when testing on a fresh install, I got the following error :
# "Configuration objects provided by have unmet dependencies in
# core/lib/Drupal/Core/Config/UnmetDependenciesException".

# Update 2017-02-16 : Back to features-based rebuild attempts.
# Apparently, still no luck : UnmetDependenciesException: Configuration objects
# provided by 'drupacompta_document' have unmet dependencies:
# field.field.node.document.field_emitter (node.type.person)
# But, the file exists :
# web/modules/custom/drupacompta_document/config/install/field.field.node.document.field_emitter.yml
# → What now ?

drush en drupacompta_core -y
