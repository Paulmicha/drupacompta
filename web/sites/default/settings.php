<?php

/**
 * @file
 * Drupal default configuration file.
 */

$databases = array();
$config_directories = array();
$settings['hash_salt'] = 'Et0edF-w65-IIXG7krLQmecj81frAQ0-S99LIUUezAA8rgCds7FCFKPjlkR97h67V4KIVeEpbw';
$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['file_private_path'] = '../private';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Default and local config files path (Git-ignored).
$config_directories['sync'] = 'sites/default/files/config_' . $settings['hash_salt'] . '/sync';

// Instance type specific config files paths (versionned).
$config_directories['dev'] = '../config/dev';
$config_directories['stage'] = '../config/stage';
$config_directories['prod'] = '../config/prod';

/**
 * Load local development override configuration, if available.
 *
 * Use settings.local.php to override variables on secondary (staging,
 * development, etc) installations of this site. Typically used to disable
 * caching, JavaScript/CSS compression, re-routing of outgoing emails, and
 * other things that should not happen on development and testing sites.
 *
 * Keep this code block at the end of this file to take full effect.
 */

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
